const express = require('express');
const app = express();
const { PORT = 5000 } = process.env;
const cors = require('cors');

// Middleware
app.use(express.json());
app.use( cors() );

// Base Endpoint.
app.get('/', (req, res) => {
    return res.status(200).send('Im a fancy Movie API.');
});

// http://localhost:3000/v1/movies -> movies []
// http://localhost:3000/v1/genres -> genres []
// http://localhost:3000/v1/users -> genres []

// Import routes from ./routes/*
app.use('/v1/movies', require('./routes/movie.routes'));
app.use('/v1/genres', require('./routes/genre.routes'));
app.use('/v1/users', require('./routes/user.routes'));

app.listen(PORT, () => console.log('Server started on port ' + PORT));