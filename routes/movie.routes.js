const router = require('express').Router();
const db = require('../db.config');
const movieFactory = require('../factories/movie');

// Get movies
router.get('/', (req, res) => {
    
    const movies = movieFactory(db).getAll();

    return res.status(200).json({
        success: true,
        data: movies
    });
});

// Get a specific movie with ID
router.get('/:movieId', (req, res) => {
    const { movieId } = req.params;
    const movie = movieFactory(db).getBy('id', movieId);
    return res.status(200).json({
        success: true,
        data: movie
    });
});

// Get a specific movie with ID
router.get('/genre/:genre', (req, res) => {
    const { genre } = req.params;
    const movies = movieFactory(db).getByGenre(genre);
    return res.status(200).json({
        success: true,
        count: movies.size(),
        data: movies
    });
});

// Post a new movie
router.post('/', (req, res) => {

    const { movie } = req.body;

    if (!movie) {
        return res.status(404).json({
            success: false,
            error: 'No movie was sent.'
        });
    }

    const id = movieFactory(db).createMovie(movie);

    if (id === false) {
        return res.status(409).json({
            success: false,
            error: `The movie ${movie.title} has already been added.`
        });
    }

    return res.status(201).json({
        success: true,
        data: id
    });

});

router.delete('/:movieId', (req, res) => {
    const { movieId } = req.params;
    movieFactory(db).delete(movieId);
    return res.status(410).json({
        success: true
    });
})

module.exports = router;