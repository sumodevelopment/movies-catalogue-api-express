const router = require('express').Router();
const db = require('../db.config');
const userFactory = require('../factories/user');

router.get('/', (req, res) => {

	const users = userFactory(db).getAll()

	return res.status(200).json({
		success: true,
		data: users.map(u => ({
			...u,
			password: null,
			hashData: null
		}))
	});
});

router.get('/:userId', (req, res) => {
	const { userId } = req.params;
	const user = userFactory(db).getBy('id', userId)
	return res.status(200).json({
		success: true,
		data: {
			...user,
			password: null,
			hashData: null
		}
	});
});

router.get('/:username/profile', (req, res) => {
	const { username } = req.params;
	const user = userFactory(db).getBy('username', username).value()

	return res.status(200).json({
		success: true,
		data: {
			...user,
			hashData: null,
		}
	});
})

router.post('/register', (req, res) => {

	const { user } = req.body;

	if (!user || !user.username || !user.password) {
		return res.status(400).json({
			success: false,
			error: 'No or invalid user data received'
		});
	}

	const { error = false, data } = userFactory(db).register(user.username, user.password);

	if (error !== false) {
		return res.status(403).json({
			success: false,
			error
		});
	}

	return res.status(201).json({
		success: true,
		data
	});
});

router.post('/login', (req, res) => {


	const { user } = req.body;

	if (!user || !user.username || !user.password) {
		return res.status(400).json({
			success: false,
			error: 'No user data received'
		});
	}

	const { error = false, success, data = -1 } = userFactory(db).login(user.username, user.password);

	if (error) {
		return res.status(403).json({
			error: error,
			success: false
		});
	}

	return res.status(200).json({
		success: true,
		data
	});


});

router.get('/:userId/favourites', (req, res) => {
	const { userId } = req.params;
	const favourites = userFactory(db).getUserFavourites(userId);

	return res.status(200).json({
		success: true,
		data: favourites
	});

})

router.post('/favourites', (req, res) => {

	const { userId, movieId } = req.body;
	const { error = false, data } = userFactory(db).addUserFavourite(userId, movieId);

	if (error !== false) {
		return res.status(400).json({
			success: false,
			error
		});
	}

	return res.status(200).json({
		success: true,
		data
	});

})

module.exports = router;