const FileSync = require('lowdb/adapters/FileSync');
const lowdb = require('lowdb');

const adapter = new FileSync('db.json');
const db = lowdb(adapter);

const database = db.getState();

if (!database) {
    db.defaults({ movies: [], genres: [], users:[] }).write();
}

module.exports = db;