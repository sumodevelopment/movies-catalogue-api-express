'use strict';
const crypto = require('crypto');

// Hash using random salts.
const hashTools = (crypto) => ({
    createRandomSalt: (length) => {
        return crypto.randomBytes( Math.ceil(length / 2) )
        .toString( 'hex' )
        .slice( 0, length );
    },
    hashPassword: (password, salt) => {
        const hash = crypto.createHmac( 'sha512', salt );
        hash.update( password );
        const passwordHash = hash.digest('hex');
        return {
            salt,
            passwordHash
        }
    }
});

/**
 * @param { crypto } crypto 
 * @param { hashTools } hashTools 
 * @returns { function } 
 */
const saltPasswordHash = (crypto, hashTools) => (userPassword) => {
    const { createRandomSalt, hashPassword } = hashTools(crypto);
    const salt = createRandomSalt( 16 );
    return hashPassword( userPassword, salt );
}

/**
 * 
 * @param { crypto } crypto 
 * @param { hashTools } hashTools 
 * @returns { function }
 */
const validatePassword = (crypto, hashTools) => (hashData, password) => {
    const { hashPassword } = hashTools(crypto);
    const hashedPassword = hashPassword( password, hashData.salt );
    return hashedPassword.passwordHash === hashData.passwordHash;
}

module.exports = {
    passwordHasher: saltPasswordHash(crypto, hashTools),
    passwordValidator: validatePassword(crypto, hashTools)
};

// Modified implementation of below:
// SOURCE: https://ciphertrick.com/salt-hash-passwords-using-nodejs-crypto/