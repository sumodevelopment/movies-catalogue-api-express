const { getterFactory, setterFactory, deletorFactory } = require('./db');
const { v4: uuidv4 } = require('uuid');

const genreSetterFactory = (state) => ({
    ...setterFactory,
    createGenre: (data) => {

        const hasGenre = state.db.get( state.table ).find({ title: data.title }).value();

        if (hasGenre) {
            return {
                error: `${ data.title } has already been added`
            };
        }

        return { 
            data: setterFactory(state).create(data) 
        }
    }
})

const genres = (uuid) => db => {
    const state = {
        db,
        uuid,
        table: 'genres'
    };

    return Object.assign(
        {},
        getterFactory(state),
        genreSetterFactory(state),
        deletorFactory(state)
    );
}

module.exports = genres(uuidv4);