const { getterFactory, setterFactory } = require('./db');
const { v4: uuidv4 } = require('uuid');
const { passwordHasher, passwordValidator } = require('../utils/passwords');

const userAuthFactory = state => ({
	login: (username, password) => {

		const userInDb = state.db.get(state.table).find({ username }).value();

		if (!userInDb) {
			return {
				error: `${username} not found`
			};
		}

		if (!passwordValidator(userInDb.hashData, password)) {
			return {
				error: `Incorrect login credentials...`
			};
		}

		state.db.get(state.table)
			.find({ username: username.toLowerCase() })
			.assign({ lastLogin: new Date().getTime() })
			.write();

		return {
			success: true,
			data: {
				...userInDb,
				hashData: null
			}
		};
	},
	register: (username, password) => {

		const existsInDb = getterFactory(state).getBy('username', username.toLowerCase()).value();

		if (existsInDb && existsInDb.username) {
			return {
				error: `The user ${username} already exists...`
			};
		}

		const hashData = passwordHasher(password);

		const newUser = {
			username: username,
			firstName: "",
			lastName: "",
			hashData,
			favourites: [],
			address: {
				street1: "",
				street2: "",
				country: "",
				"postalCode": ""
			},
			active: 1,
			createdAt: new Date().getTime(),
			lastLogin: new Date().getTime()
		}

		const userId = setterFactory(state).create(newUser);

		return {
			success: true,
			data: {
				id: userId,
				...newUser,
				hashData: null
			}
		};
	}
});

const favouritesFactory = state => ({
	getUserFavourites: userId => {
		const { favourites = [] } = getterFactory(state).getBy('id', userId).value();
		const favouriteMovies = favourites.map(movieId => {
			return state.db.get('movies').find({ id: movieId }).value()
		}) || [];
		return favouriteMovies;
	},
	addUserFavourite: (userId, movieId) => {
		const { favourites = [] } = getterFactory(state).getBy('id', userId).value();

		if (favourites.includes(movieId)) {
			return {
				error: `The movie is already in your favourites`
			}
		}

		getterFactory(state)
			.getBy('id', userId)
			.assign({
				favourites: [...favourites, movieId]
			})
			.write();
		return {
			success: true,
			data: [...favourites, movieId]
		};
	}
});

const users = uuid => db => {

	const state = {
		uuid,
		db,
		table: 'users'
	};

	return Object.assign(
		{},
		getterFactory(state),
		userAuthFactory(state),
		favouritesFactory(state)
	);
}

module.exports = users(uuidv4);