// Getter Factory
const getterFactory = state => ({
    getAll: ()=> {
        return state.db.get(state.table).filter({ active: 1 });
    },
    getBy: (key, value) => {
        return state.db.get(state.table).find(record => record[key].toString().toLowerCase() === value.toString().toLowerCase());
    }
});

// Setter Factory
const setterFactory = state => ({
    create: data => {
        const id = state.uuid();
        console.log( 'Creating  a new ' + state.table );
        const newRecord = {
            id,
            ...data
        }
        console.log( newRecord );
        state.db.get( state.table ).push({
            id,
            ...data
        }).write();
        return id;
    }
});

// Deletor Factory
const deletorFactory = state => ({
    delete: id => {
        return state.db.get(state.table)
                       .find({ id: id })
                       .assign({ active: 0 })
                       .write();
    }
})

module.exports = {
    getterFactory,
    setterFactory,
    deletorFactory
}